﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RiotApi.Model
{
    public class FellowPlayer
    {
        public int championId { get; set; }
        public int teamId { get; set; }
        public int summonerId { get; set; }
    }
}
