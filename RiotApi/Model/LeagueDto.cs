﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RiotApi.Model
{
   public  class LeagueDto
    {
        public string queue { get; set; }
        public string name { get; set; }
        public string participantId { get; set; }
        public List<Entry> entries { get; set; }
        public string tier { get; set; }
    }
}
