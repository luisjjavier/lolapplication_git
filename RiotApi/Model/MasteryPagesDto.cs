﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RiotApi.Model
{
    public class MasteryPagesDto
    {
        public List<Page> pages { get; set; }
        public int summonerId { get; set; }
    }
}
