﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RiotApi.Model
{
    public class MiniSeries
    {
        public string progress { get; set; }
        public int target { get; set; }
        public int losses { get; set; }
        public int wins { get; set; }
    }
}
