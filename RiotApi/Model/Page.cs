﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RiotApi.Model
{
   public  class Page
    {
        public List<Mastery> masteries { get; set; }
        public int id { get; set; }
        public string name { get; set; }
        public bool current { get; set; }
    }
}
