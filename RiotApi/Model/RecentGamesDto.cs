﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RiotApi.Model
{
   public class RecentGamesDto
    {
        public List<Game> games { get; set; }
        public int summonerId { get; set; }
    }
}
