﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace lolChampions.Model
{
    public class Champion
    {
        public string version { get; set; }
        public string id { get; set; }
        public string name { get; set; }
        public string title { get; set; }
        public string blurb { get; set; }
        public Info info { get; set; }
        public Image image { get; set; }
        public string[] tags { get; set; }
        public string partype { get; set; }
        public Stats stats { get; set; }
    }
}
