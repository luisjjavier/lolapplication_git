﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace lolChampions.Model
{
   public  class Image
    {
       public string full { get; set; }
        public string sprite { get; set; }
        public string group { get; set; }
        public int x { get; set; }
       public int y { get; set; }
       public int w { get; set; }
       public int h { get; set; }

                
    }
}
