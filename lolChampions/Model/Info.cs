﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace lolChampions.Model
{
    public class Info
    {
        public int attack { get; set; }
        public int defense { get; set; }
        public int magic { get; set; }
        public int difficulty { get; set;}
    }
}
