﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace lolChampions.Model
{
   public class Stats
    {
        public decimal hp { get; set; }
        public decimal hpperlevel { get; set; }
        public decimal mp{ get; set; }
        public decimal mpperlevel { get; set; }
        public decimal movespeed { get; set; }
        public decimal armor { get; set; }
        public decimal armorperlevel { get; set; }
        public decimal spellblock { get; set; }
        public decimal spellblockperlevel { get; set; }
        public decimal attackrange { get; set; }
        public decimal hpreng { get; set; }
        public decimal hprengperlevel { get; set; }
        public decimal mpregen { get; set; }
        public decimal mpregenperlevel { get; set; }
        public decimal crit { get; set; }
        public decimal critperlevel { get; set; }
        public decimal attackdamage { get; set; }
        public decimal attackdamageperlevel { get; set; }
        public decimal attackspeedoffset { get; set; }
        public decimal attackspeedperlevel { get; set; }
    }
}
