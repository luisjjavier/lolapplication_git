﻿using _360Political.Common.Utils;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using lolChampions.Model;
using System.Net;
using System.IO;

namespace lolChampions
{
    class Program
    {
        static void Main(string[] args)
        {
            HttpRESTSimpleClient httpclient = new HttpRESTSimpleClient("http://ddragon.leagueoflegends.com/cdn/4.20.1/data");
            RestResponse response = new RestResponse();

            Carga ListOfUsers = httpclient.Invoke<Carga>(HttpMethods.GET, string.Format("/en_US/champion.json"), response.Initialize);
            Console.WriteLine(ListOfUsers.data.Count);
            foreach (var i in ListOfUsers.data)
            {
                Directory.CreateDirectory(string.Format(@"E:\FotosPrueba\{0}",i.Key.ToString()));
                for (int x = 0; x < 10; x++)

                {
                    HttpWebRequest request = (HttpWebRequest)WebRequest.Create(string.Format("http://ddragon.leagueoflegends.com/cdn/img/champion/splash/{0}_{1}.jpg", i.Key.ToString(),x));
                    try
                    {
                        if (((HttpWebResponse)request.GetResponse()).StatusCode == HttpStatusCode.NotFound)
                        {
                            break;
                        }
                    }
                    catch (Exception)
                    {
                        Console.WriteLine(string.Format("El campeon : {0} y tiene {1} skin",i.Key.ToString(),x+1));
                        break;
                    }
                  
                    HttpWebResponse response2 = (HttpWebResponse)request.GetResponse();


                    if ((response2.StatusCode == HttpStatusCode.OK && response2.ContentType.StartsWith("image", StringComparison.OrdinalIgnoreCase)))
                    {

                       
                        using (Stream inputStream = response2.GetResponseStream())
                        {

                            using (Stream outputStream = File.OpenWrite(string.Format(@"E:\FotosPrueba\{0}\{1}.jpg", i.Key.ToString(),x)))
                            {
                                byte[] buffer = new byte[4096];
                                int bytesRead;
                                do
                                {
                                    bytesRead = inputStream.Read(buffer, 0, buffer.Length);
                                    outputStream.Write(buffer, 0, bytesRead);
                                } while (bytesRead != 0);
                            }
                        }
                    }
                    else {
                        break;
                    }
                    
                    
                }
           
            }
            Console.ReadLine();
        }
    }
}
