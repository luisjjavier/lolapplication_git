﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace  _360Political.Common.Utils
{
    public class CallResult
    {
        public bool Success { get; set; }
        public string Message { get; set; }
        public System.Exception error { get; set; }
        public bool Exist { get; set; }
    }
}
